# Dice-So-Pathfinder

A dodgy fork of Dice-So-Nice by Simone with a Pathfinder 2e theme.

<img src="sample.png" alt="Sample" width="590" height="460">

<p></p>

**Installation**

Dice-So-Pathfinder can be installed through the Foundry installer or by using the following manifest:

[https://gitlab.com/mistamichal/dice-so-pathfinder/-/raw/master/module.json](https://gitlab.com/mistamichal/dice-so-pathfinder/-/raw/master/module.json)